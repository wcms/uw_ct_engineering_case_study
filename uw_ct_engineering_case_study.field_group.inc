<?php

/**
 * @file
 * uw_ct_engineering_case_study.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_engineering_case_study_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_case_study_image_upload|node|uw_engineering_case_study|form';
  $field_group->group_name = 'group_case_study_image_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_engineering_case_study';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '5',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-case-study-image-upload field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_case_study_image_upload|node|uw_engineering_case_study|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_case_study_synopsis|node|uw_engineering_case_study|form';
  $field_group->group_name = 'group_case_study_synopsis';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_engineering_case_study';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Synopsis',
    'weight' => '4',
    'children' => array(
      0 => 'field_case_study_authors',
      1 => 'field_case_revision_date',
      2 => 'field_case_study_source',
      3 => 'field_case_study_length',
      4 => 'field_case_study_summary',
      5 => 'field_learning_objectives',
      6 => 'field_case_study_key_words',
      7 => 'field_case_study_ceab_attributes',
      8 => 'field_case_study_modules',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_case_study_synopsis|node|uw_engineering_case_study|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_case_study_upload|node|uw_engineering_case_study|form';
  $field_group->group_name = 'group_case_study_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_engineering_case_study';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload a file',
    'weight' => '7',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload a file',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-case-study-upload field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_case_study_upload|node|uw_engineering_case_study|form'] = $field_group;

  return $export;
}
