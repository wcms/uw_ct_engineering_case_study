<?php

/**
 * @file
 * uw_ct_engineering_case_study.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_engineering_case_study_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'case_study_available_block';
  $context->description = 'Available case studies search block';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cases/available-cases' => 'cases/available-cases',
        'cases/available-cases/*' => 'cases/available-cases/*',
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-6b05113274494951d1cd121489c6cd64' => array(
          'module' => 'views',
          'delta' => '6b05113274494951d1cd121489c6cd64',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Available case studies search block');
  t('Content');
  $export['case_study_available_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'case_study_retired_block';
  $context->description = 'Retired case studies search block';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cases/retired-cases' => 'cases/retired-cases',
        'cases/retired-cases/*' => 'cases/retired-cases/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-d0a202ea0a3a25e3a6f0a78afd1ef9a8' => array(
          'module' => 'views',
          'delta' => 'd0a202ea0a3a25e3a6f0a78afd1ef9a8',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Retired case studies search block');
  $export['case_study_retired_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'case_study_upcoming_block';
  $context->description = 'Upcoming case studies search block';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cases/upcoming-cases' => 'cases/upcoming-cases',
        'cases/upcoming-cases/*' => 'cases/upcoming-cases/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-01605835e0cb24a12a38b232b334bc77' => array(
          'module' => 'views',
          'delta' => '01605835e0cb24a12a38b232b334bc77',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Upcoming case studies search block');
  $export['case_study_upcoming_block'] = $context;

  return $export;
}
