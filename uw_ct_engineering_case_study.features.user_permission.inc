<?php

/**
 * @file
 * uw_ct_engineering_case_study.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_engineering_case_study_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_engineering_case_study content'.
  $permissions['create uw_engineering_case_study content'] = array(
    'name' => 'create uw_engineering_case_study content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_engineering_case_study content'.
  $permissions['delete any uw_engineering_case_study content'] = array(
    'name' => 'delete any uw_engineering_case_study content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_engineering_case_study content'.
  $permissions['delete own uw_engineering_case_study content'] = array(
    'name' => 'delete own uw_engineering_case_study content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in case_release_state'.
  $permissions['delete terms in case_release_state'] = array(
    'name' => 'delete terms in case_release_state',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in case_study_ceab_attributes'.
  $permissions['delete terms in case_study_ceab_attributes'] = array(
    'name' => 'delete terms in case_study_ceab_attributes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in case_study_keywords'.
  $permissions['delete terms in case_study_keywords'] = array(
    'name' => 'delete terms in case_study_keywords',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_engineering_case_study_programs'.
  $permissions['delete terms in uw_engineering_case_study_programs'] = array(
    'name' => 'delete terms in uw_engineering_case_study_programs',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_engineering_case_study content'.
  $permissions['edit any uw_engineering_case_study content'] = array(
    'name' => 'edit any uw_engineering_case_study content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_engineering_case_study content'.
  $permissions['edit own uw_engineering_case_study content'] = array(
    'name' => 'edit own uw_engineering_case_study content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in case_release_state'.
  $permissions['edit terms in case_release_state'] = array(
    'name' => 'edit terms in case_release_state',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in case_study_ceab_attributes'.
  $permissions['edit terms in case_study_ceab_attributes'] = array(
    'name' => 'edit terms in case_study_ceab_attributes',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in case_study_keywords'.
  $permissions['edit terms in case_study_keywords'] = array(
    'name' => 'edit terms in case_study_keywords',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_engineering_case_study_programs'.
  $permissions['edit terms in uw_engineering_case_study_programs'] = array(
    'name' => 'edit terms in uw_engineering_case_study_programs',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
